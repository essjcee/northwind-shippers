package com.example.demo.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.demo.entities.Shipper;

@Repository("test")
public class TestRepository implements ShipperRepository{

	@Override
	public List<Shipper> getAllShippers() {
		// TODO Auto-generated method stub
		List<Shipper> shippers = null;
		shippers = new ArrayList<Shipper>();
		for(int i = 1; i <= 10; i++) {
			shippers.add(new Shipper(i, "Shipper " + i, "(1234) 56789" + i));
		}
		return shippers;		
	}

	@Override
	public Shipper getShipperById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Shipper editShipper(Shipper shipper) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deleteShipper(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Shipper addShipper(Shipper shipper) {
		// TODO Auto-generated method stub
		return null;
	}

}
