package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.Shipper;

@Component
public interface ShipperRepository {
	public List<Shipper> getAllShippers();
	public Shipper getShipperById(int id); 
	public Shipper editShipper(Shipper shipper);
	public int deleteShipper(int id);
	public Shipper addShipper(Shipper shipper);
}
